FROM node:12-alpine

RUN apk add --update curl bash
RUN npm install elasticdump -g

CMD [ "elasticdump", "--help" ]
